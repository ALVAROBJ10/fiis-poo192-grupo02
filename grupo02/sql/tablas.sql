create table usuario(
	nombreusuario text,
	correousuario varchar(64) primary key,
	passwordusuario varchar(30),
	telefono varchar(11)
);

create table libro(
	codigolibro varchar(20) primary key,
	nombrelibro text,
	autor text,
	precio float,
	editorial text,
	edicion integer,
	categoria text,
	idioma text,
	resumen text
);

create table tarjetacredito(
	numerotarjeta varchar(16) primary key,
	nombreusuario1 text,
	correousuario1 varchar(64) references usuario,
	cvc varchar(3)
);

create table detallecompra(
	correousuario2 varchar(64) references usuario,
	codigolibro1 varchar(20) references libro
);


/*USUARIO
*/
insert into usuario values('Orfeo Fernández Ochoa','ofernandezo@uni.pe','orfeofernandez','51939314696',);
insert into usuario values('Alvaro Julon Gonzales','alvarobj10@gmail.com','alvarojulon','51962758941');
insert into usuario values('Henry Berroa Escriba','sois2004@hotmail.com','henryberroa','51991936548');
insert into usuario values('Siler Mendoza Salazar','silermendoza8@gmail.com','silermendoza','51933665699');
insert into usuario values('Juan Vargas Mendoza','juanvargas18@gmail.com','juanvargas','51954856999')



/*LIBRO
*/

insert into libro values('CI00001','Community Ecology','Peter J. Morin',25.90,'Wiley-Blackwell',2,'Ciencias','Inglés',NULL);
insert into libro values('CI00002','Industrial Dynamics','Jay W. Forrester',31.99,'Pegasus',2,'Ciencias','Inglés',NULL);
insert into libro values('CI00003','Teoría General de los Sistemas','Ludwig Von Bertalanffy',12.90,'Fondo de Cultura Económica',4,'Ciencias','Español',NULL);

insert into libro values('CS00001','Anatomía Humana para estudiantes de ciencias de la salud','Juan Suarez Quintanilla',25.95,'ELSEVIER',4,'Ciencias de la salud','Español',NULL);
insert into libro values('CS00001','Principles and Practice of Hospital Medicine','Sylvia C. McKean',30.90,'Mc-Graw Hill',2,'Ciencias de la salud','Inglés',NULL);
insert into libro values('CS00001','Vademécum Académico de Medicamentos','Rodolfo Rodríguez Carranza',25.90,'Mc-Graw Hill',6,'Ciencias de la salud','Español',NULL);

insert into libro values('COM00001','A Tour of C++','Bjarne Stroustrup',35.90,'Addison-Wesley',4,'Computación','Inglés',NULL);
insert into libro values('COM00002','Inteligencia Artificial con Aplicaciones a la Ingeniería','Pedro Ponce Cruz',21.90,'Alfaomega',4,'Computación','Español',NULL);
insert into libro values('COM00003','Java The Complete Reference','Herbert Schildt',35.90,'Oracle Press',11,'Computación','Inglés',NULL);
insert into libro values('COM00004','The C++ Programming Language','Bjarne Stroustrup',30.90,'Addison-Wesley',5,'Computación','Inglés',NULL);

insert into libro values('CE00001','Economic Indicators','Philip Mohr',23.85,'UNISA Press',4,'Economía','Inglés',NULL);
insert into libro values('CE00002','Economic Theory','Gary S. Becker',28.90,'Routledge',5,'Economía','Inglés',NULL);
insert into libro values('CE00003','Microeconomía','Robert Pindyck',25.90,'Prentice Hall',7,'Economía','Inglés',NULL);
insert into libro values('CE00004','Principios de Economía','Gregory Mankiw',25.90,'Cengage Learning',7,'Economía','Español',NULL);

insert into libro values('FIS00001','Física Volumen I','Richard Feynman',16.50,'Fondo Educativo Interamericano',2,'Física','Español',NULL);
insert into libro values('FIS00002','La teoría del todo','Stephen Hawking',14.90,'DeBolsillo',3,'Física','Español',NULL);
insert into libro values('FIS00003','Principios Matemáticos de la Filosofía Natural','Isaac Newton',22.90,'Alianza Editorial',2,'Física','Español',NULL);

insert into libro values('MAT00001','Cálculo en Variedades','Michael Spivak',20.90,'Reverte',2,'Matemática','Español',NULL);
insert into libro values('MAT00002','Calculus','Michael Spivak',25.90,'Reverte',3,'Matemática','Español',NULL);
insert into libro values('MAT00003','Mathematical Analysis I','Vladimir Zorich',45.90,'Springer',2,'Matemática','Inglés',NULL);

insert into libro values('QUI00001','Principios de Química Orgánica','T. A. Geissman',18.90,'Reverte',2,'Química','Español',NULL);
insert into libro values('QUI00002','Química General','Petrucci',25.90,'Peason',10,'Química','Español',NULL);
insert into libro values('QUI00003','Química La Ciencia Central','Brown',25.90,'Pearson',12,'Química','Español',NULL);