package edu.uni.fiis.poo.grupo02.bean;

public class Detallecompra {
    private String correousuario2;
    private String codigolibro1;

    public Detallecompra(String correousuario2, String codigolibro1){
        this.correousuario2 = correousuario2;
        this.codigolibro1 = codigolibro1;
    }

    public String getCorreousuario2() {
        return correousuario2;
    }

    public String getCodigolibro1() {
        return codigolibro1;
    }
}
