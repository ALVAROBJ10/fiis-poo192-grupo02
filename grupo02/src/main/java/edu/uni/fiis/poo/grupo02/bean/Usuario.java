package edu.uni.fiis.poo.grupo02.bean;

public class Usuario {
    private String nombreusuario;
    private String correousuario;
    private String passwordusuario;
    private String telefono;

    public Usuario(String nombreusuario, String correousuario, String passwordusuario, String telefono){
        this.nombreusuario = nombreusuario;
        this.correousuario = correousuario;
        this.passwordusuario = passwordusuario;
        this.telefono = telefono;
    }
    public Usuario(String correousuario){
        this.correousuario = correousuario;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public String getCorreousuario() {
        return correousuario;
    }

    public String getPasswordusuario() {
        return passwordusuario;
    }

    public String getTelefono() {
        return telefono;
    }
}
