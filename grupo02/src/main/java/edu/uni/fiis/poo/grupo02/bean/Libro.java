package edu.uni.fiis.poo.grupo02.bean;

public class Libro {
    private String codigolibro;
    private String nombrelibro;
    private String autor;
    private Float precio;
    private String editorial;
    private Integer edicion;
    private String categoria;
    private String idioma;
    private String resumen;

    public Libro(String codigolibro, String nombrelibro, String autor, Float precio, String editorial, Integer edicion, String categoria, String idioma, String resumen){
        this.codigolibro = codigolibro;
        this.nombrelibro = nombrelibro;
        this.autor = autor;
        this.precio = precio;
        this.editorial = editorial;
        this.edicion = edicion;
        this.categoria = categoria;
        this.idioma = idioma;
        this.resumen = resumen;
    }

    public Libro(String codigolibro){
        this.codigolibro = codigolibro;
    }

    public String getCodigolibro() {
        return codigolibro;
    }

    public String getNombrelibro() {
        return nombrelibro;
    }

    public String getAutor() {
        return autor;
    }

    public Float getPrecio() {
        return precio;
    }

    public String getEditorial() {
        return editorial;
    }

    public Integer getEdicion() {
        return edicion;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getIdioma() {
        return idioma;
    }

    public String getResumen() {
        return resumen;
    }
}
