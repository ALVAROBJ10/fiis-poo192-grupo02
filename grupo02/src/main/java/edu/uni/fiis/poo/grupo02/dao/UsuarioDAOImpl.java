package edu.uni.fiis.poo.grupo02.dao;

import edu.uni.fiis.poo.grupo02.bean.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO {
    @Autowired
    private JdbcTemplate template;

    public List<Usuario> mostrarUsuarios() throws Exception{
        List<Usuario> resp = new ArrayList<>();
        Connection conn = template.getDataSource().getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM USUARIO");
        while(rs.next()){
            Usuario usuario = new Usuario(rs.getString(1), rs.getString(2),
                    rs.getString(3), rs.getString(4));
            resp.add(usuario);
        }
        rs.close();
        st.close();
        conn.close();
        return resp;
    }

    public int mostrarCantidadUsuarios() throws Exception {
        Connection conn = template.getDataSource().getConnection();
        String sql = "SELECT COUNT(*) FROM USUARIO";
        int cantidad = template.queryForObject(sql, Integer.class);
        conn.close();
        return cantidad;
    }

    public void agregarUsuario(Usuario usuario) throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String sql = "INSERT INTO USUARIO(NOMBREUSUARIO, CORREOUSUARIO, PASSWORDUSUARIO, TELEFONO) VALUES(?, ?, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1,usuario.getNombreusuario());
        pst.setString(2,usuario.getCorreousuario());
        pst.setString(3,usuario.getPasswordusuario());
        pst.setString(4,usuario.getTelefono());
        pst.executeUpdate();
        pst.close();
        conn.close();
    }

    public void eliminarUsuario(String correousuario) throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String sql = "DELETE FROM USUARIO WHERE CORREOUSUARIO = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(2,correousuario);
        pst.executeUpdate();
        pst.close();
        conn.close();
    }

    public List<Usuario> buscarUsuario(String correousuario) throws Exception{
        List<Usuario> resp = new ArrayList<>();
        Connection conn = template.getDataSource().getConnection();
        String sql = "SELECT * FROM USUARIO WHERE CORREOUSUARIO LIKE ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(2,"%" + correousuario + "%");
        ResultSet rs = pst.executeQuery();
        while (rs.next()){
            Usuario usuario = new Usuario(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4));
            resp.add(usuario);
        }
        rs.close();
        pst.close();
        conn.close();
        return resp;
    }

}
