package edu.uni.fiis.poo.grupo02.service;

import edu.uni.fiis.poo.grupo02.bean.Libro;
import edu.uni.fiis.poo.grupo02.dao.LibroDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroService {
    @Autowired
    LibroDAO libroDAO;

    public List<Libro> mostrarLibros() throws Exception{
        return libroDAO.mostrarLibros();
    }
    public int mostrarCantidadLibros() throws Exception{
        return libroDAO.mostrarCantidadLibros();
    }
    public void agregarLibro(Libro libro) throws Exception{
        libroDAO.agregarLibro(libro);
    }
    public void eliminarLibro(String codigolibro) throws Exception{
        libroDAO.eliminarLibro(codigolibro);
    }
    public String buscarLibro(String codigolibro) throws Exception{
        return libroDAO.buscarLibro(codigolibro);
    }
}
