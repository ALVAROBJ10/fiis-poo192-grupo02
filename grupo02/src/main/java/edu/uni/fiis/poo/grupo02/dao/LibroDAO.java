package edu.uni.fiis.poo.grupo02.dao;

import edu.uni.fiis.poo.grupo02.bean.Libro;

import java.util.List;

public interface LibroDAO {
    public List<Libro> mostrarLibros() throws Exception;
    public int mostrarCantidadLibros() throws Exception;
    public void agregarLibro(Libro libro) throws Exception;
    public void eliminarLibro(String codigolibro) throws Exception;
    public String buscarLibro(String codigolibro) throws Exception;
}
