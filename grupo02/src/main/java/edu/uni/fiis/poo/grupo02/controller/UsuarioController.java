package edu.uni.fiis.poo.grupo02.controller;

import edu.uni.fiis.poo.grupo02.bean.Usuario;
import edu.uni.fiis.poo.grupo02.request.UsuarioRequest;
import edu.uni.fiis.poo.grupo02.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsuarioController{
    @Autowired
    UsuarioService usuarioService;

    @RequestMapping("/usuarios")
    public List<Usuario> mostrarUsuarios() throws Exception{
        return usuarioService.mostrarUsuarios();
    }

    @RequestMapping("/usuarios/cantidad")
    public String mostrarCantidadUsuarios() throws Exception{
        return "<h1> Tenemos " +  usuarioService.mostrarCantidadUsuarios() + " usuarios registrados </h1>";
    }

    @PostMapping("/usuarios/agregar")
    public void agregarUsuario(@RequestBody UsuarioRequest req) throws Exception{
        Usuario usuario = new Usuario(req.getNombreusuario(),req.getCorreousuario(),req.getPasswordusuario(),req.getTelefono());
        usuarioService.agregarUsuario(usuario);
    }

    @PostMapping("/usuarios/eliminar")
    public String eliminarUsuario(@RequestParam String correousuario) throws Exception{
        Usuario usuario = new Usuario(correousuario);
        usuarioService.eliminarUsuario(correousuario);
        return "<h1> Usuario eliminado con éxito </h1>";
    }

    @RequestMapping("/usuarios/buscar")
    public List<Usuario> buscarUsuario(@RequestParam String correousuario) throws Exception{
        System.out.println(correousuario);
        return usuarioService.buscarUsuario(correousuario);
    }

}
