package edu.uni.fiis.poo.grupo02.service;

import edu.uni.fiis.poo.grupo02.bean.Usuario;
import edu.uni.fiis.poo.grupo02.dao.UsuarioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    UsuarioDAO usuarioDAO;

    public List<Usuario> mostrarUsuarios() throws Exception{
        return usuarioDAO.mostrarUsuarios();
    }

    public int mostrarCantidadUsuarios() throws Exception{
        return usuarioDAO.mostrarCantidadUsuarios();
    }

    public void agregarUsuario(Usuario usuario) throws Exception{
        usuarioDAO.agregarUsuario(usuario);
    }

    public void eliminarUsuario(String correousuario) throws Exception{
        usuarioDAO.eliminarUsuario(correousuario);
    }

    public List<Usuario> buscarUsuario(String correousuario) throws Exception{
        return usuarioDAO.buscarUsuario(correousuario);
    }

}
