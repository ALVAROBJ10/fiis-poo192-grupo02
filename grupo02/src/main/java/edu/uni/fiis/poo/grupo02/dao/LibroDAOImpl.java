package edu.uni.fiis.poo.grupo02.dao;

import edu.uni.fiis.poo.grupo02.bean.Libro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LibroDAOImpl implements LibroDAO{
    @Autowired
    private JdbcTemplate template;

    public List<Libro> mostrarLibros() throws Exception{
        List<Libro> resp = new ArrayList<>();
        Connection conn = template.getDataSource().getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM LIBRO");
        while(rs.next()){
            Libro libro = new Libro(rs.getString(1), rs.getString(2), rs.getString(3),
                                    rs.getFloat(4), rs.getString(5), rs.getInt(6),
                                    rs.getString(7), rs.getString(8), rs.getString(9));
            resp.add(libro);
        }
        rs.close();
        st.close();
        conn.close();
        return resp;
    }

    public int mostrarCantidadLibros() throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String sql = "SELECT COUNT(*) FROM LIBRO";
        int cantidad = template.queryForObject(sql, Integer.class);
        conn.close();
        return cantidad;
    }

    public void agregarLibro(Libro libro) throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String sql = "INSERT INTO LIBRO VALUES(?, ?, ?, ?)";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1,libro.getCodigolibro());
        pst.setString(2,libro.getNombrelibro());
        pst.setString(3,libro.getAutor());
        pst.setFloat(4,libro.getPrecio());
        pst.setString(5,libro.getEditorial());
        pst.setInt(6,libro.getEdicion());
        pst.setString(7,libro.getCategoria());
        pst.setString(8,libro.getIdioma());
        pst.setString(9,libro.getResumen());
        pst.executeUpdate();
        pst.close();
        conn.close();
    }

    public void eliminarLibro(String codigolibro) throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String sql = "DELETE FROM LIBRO WHERE CODIGOLIBRO = ?";
        PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1,codigolibro);
        pst.executeUpdate();
        pst.close();
        conn.close();
    }

    public String buscarLibro(String codigolibro) throws Exception{
        Connection conn = template.getDataSource().getConnection();
        String sql = "SELECT COUNT(*) FROM LIBRO WHERE CODIGOLIBRO = '" + codigolibro + "'";
        int cant = template.queryForObject(sql, Integer.class);
        String mensaje = "";
        if(cant == 0){
            mensaje = "Libro no se encuentra registrado";
        }else{
            mensaje = "Libro se encuentra registrado";
        }
        return mensaje;
    }
}
